# Дипломна работа към Тракийския университет
Дова е домът на кода, с който се изгражда дипломната ми работа за учител по информатика и информационни технологии към Тракийския университет в Стара Загора.
А [това е вече изграденият ѝ образ като web страница](https://gnx.gitlab.io/nvo-7-matematika/).

Тази разработка е лицензирана под
[Криейтив Комънс Признание-Споделяне на споделеното 2.5 България][cc-by-sa].

[![CC BY-SA 2.5][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/2.5/bg/
[cc-by-sa-image]: https://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by-sa.svg
